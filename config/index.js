const dbConfig = require('./database.config');

const successfulConnection = () => {
    console.log('Successfully connected to the database');
}

const connectionFailed = err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
}

module.exports = {
    dbConfig,
    successfulConnection,
    connectionFailed
}
