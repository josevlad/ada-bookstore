module.exports = {
    url: 'mongodb://localhost:27017/ada-ebooks',
    options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    }
}
