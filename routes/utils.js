const queryDbBuilder = (params) => {
    if (Object.keys(params).length === 0)
        return {};

    const mongoQuery = {
        $and: [] // se debe cumplir todas las condiciones
        // $or : [] // se debe cumplir almenos una
        // $not: [] // es el inverso de $and
    }

    if (params.author) {
        let regExpAuthor = new RegExp(params.author, 'i');
        mongoQuery.$and.push({author: regExpAuthor});
    }

    if (params.title) {
        let regExpTitle = new RegExp(params.title, 'i');
        mongoQuery.$and.push({title: regExpTitle});
    }

    if (params.currency) {
        // mongoQuery.$and.push({ price: { currency: params.currency } }); 4.4
        mongoQuery.$and.push({ 'price.currency': params.currency });
    }

    if (params.ltePrice) { // less than or equal to (i.e. <=)
        mongoQuery.$and.push({ 'price.value': { $lte: params.ltePrice } });
    }

    if (params.gtePrice) { // greater than or equal to (i.e. >=)
        mongoQuery.$and.push({ 'price.value': { $gte: params.gtePrice } });
    }

    if (params.categories) {
        mongoQuery.$and.push({ categories: { $all: params.categories.split(',') } })
    }

    return mongoQuery.$and.length === 0 ? {} : mongoQuery;
}

module.exports = {
    queryDbBuilder
}
