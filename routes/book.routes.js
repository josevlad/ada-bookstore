const express = require('express');
const router = express.Router();
const {Book} = require('./../models');
const {queryDbBuilder} = require("./utils");

const basePath = '/books';
let response = {
    httpStatus: undefined,
    body: {}
}

router.get(basePath, (req, res) => {
    response.httpStatus = 200;
    response.body = {};

    const query = queryDbBuilder(req.query);
    const [field, orientation] = req.query.orderBy ? req.query.orderBy.split(',') : ['title', 'asc'];

    Book.find(query, null, {sort: { [`${field}`]: orientation }})
        .then(books => {
            response.body = {
                count: books.length,
                data: books
            };
            res.status(response.httpStatus).json(response);
        })
        .catch(err => {
            console.error({err});
            response.httpStatus = 500;
            response.body = {
                message: err.message || "Some error occurred while retrieving notes."
            };
            res.status(response.httpStatus).json(response);
        })
});

module.exports = router;
