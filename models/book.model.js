const mongoose = require('mongoose');

const BookSchema = mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    categories: {
        type: [String],
        required: true
    },
    price: {
        currency: {
            type: String,
            required: true
        },
        value: {
            type: Number,
            required: true
        },
        displayValue: {
            type: String,
            required: true
        }
    }
})
    .set('toJSON', {
        virtuals: true,
        versionKey: false,
        transform: (doc, ret) => {
            delete ret._id
        }
    });

module.exports = mongoose.model('Book', BookSchema);

/*

  {
    "title": "Vrienden voor het leven",
    "author": "Maeve Binchy",
    "image": "http://s.s-bol.com/imgbase0/imagebase/large/FC/5/3/6/6/1001004011806635.jpg",
    "categories": [
      "Cómics",
      "Marketing",
      "Cuentos"
    ],
    "price": {
      "currency": "GBP",
      "value": 10,
      "displayValue": "10.00"
    }
  }

*/
