const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const {dbConfig, successfulConnection, connectionFailed} = require('./config');
const routes = require('./routes');
const server = express();
const port = 3001;

mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.url, dbConfig.options)
    .then(successfulConnection)
    .catch(connectionFailed);

server.use(cors());
server.use(express.urlencoded({extended: true}));
server.use(express.json());

server.use('/', routes.bookRoutes);

server.listen(port, () => {
    console.log(`server listening on port ${port}`);
});
